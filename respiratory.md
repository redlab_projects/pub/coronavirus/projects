# Autres équipement respiratoire

## Valve pour masque décathlon (Impression 3D)

**Produit par:**
 * Lahcen

![charlotte](decathlon_mask/charlotte.png)

**Infos:**

Modèle charlotte (respirateur) pour adaption masque décatlon

**Fichiers:**

* [Archive](decathlon_mask/charlotte-valve.zip)
 
**matières premières:**

**montage:**
# Visières 

## Modèle Lahcen (laser)

![lahcen](faceshields/lahcen/lahcen.jpg)

**Produit par:**
 * lahcen
 * Labsud

**Infos:**

Flexible, Découpe laser, optimisée pour minimiser la chute de matériaux

**Fichiers:**
* [Designé par Lahcen El Jid (format AI)](faceshields/lahcen/lahcen.ai)
* [Designé par Lahcen El Jid (Format AI)](faceshields/lahcen/menzo.ai) (à valider)
* [Designé par Lahcen El Jid (format DXF)](faceshields/lahcen/menzo.dxf)

**matières premières:**

* PMMA Altu/plexi extrudé? coulé? Ou en touver ?
* PVC A4 180g?

**montage:**

facile à monter

## Modèle Albilab (laser)

![albilab](faceshields/albilab/albilab.jpg)

**Produit par:**
 * Albilab

**Infos:**

fork de bordeaux? modifié par Guillaume à Albilab

**Fichiers:**
* [Visière Albilab]("faceshields/albilab/Visière Albilab V4.DXF")

**matières premières:**

**montage:**

* PMMA Altu/plexi extrudé? coulé? Ou en touver ?
* PVC A4 180g?

## Modèle Bilbao (laser)

![bilbao](faceshields/bilbao/bilbao.jpg)

**Produit par:**
 * Rutech Rodez
 * Web5 Béziers
 * Crealab Graulhet

**Infos:**

Largement produit, fourni par Alexis Muller (français à Bilbao)

**Fichiers:**
* [Modèle S](faceshields/bilbao/DL-visiere-bilbao-S-1-pce.svg)
* [Modèle M](faceshields/bilbao/DL-visiere-bilbao-M-1-pce.svg)
* [Modèle L](faceshields/bilbao/DL-visiere-bilbao-L-1-pce.svg)

et dérivés ...

* [Modèle modifié par Yoann (info Alèze)](faceshields/bilbao/BilbaoALeze_V3.zip)

Comporte des ergots de centrage pour faciliter le maintien de la feuille

![aleze](faceshields/bilbao/schema_aleze.png)

* [Notice produite par Crealab Graulhet](faceshields/bilbao/Notice&#32Visières&#32Crealab&#32Graulhet.pdf)

![rajaa](faceshields/notice.jpg)


**matières premières:**

* PMMA Altu/plexi extrudé? coulé? Ou en touver ?
* PVC A4 200mg (Marques utilisées)
* elastiques

![pvc](faceshields/bilbao/pvc.jpg)

![elastiques](faceshields/bilbao/rubberbands.jpg)

**montage:**

ici notice de montage à venir
